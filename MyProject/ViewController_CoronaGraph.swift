//
//  ViewController_SwiftUI1.swift
//  MyProject
//
//  Created by  kpugame on 2020/06/27.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit
import SwiftUI



struct CoronaChart: View {
    var posts : NSMutableArray
    
    var body: some View {
        VStack {
            Text("코로나 사망자 수")
            HStack {
                ForEach(0..<10) { index in
                    
                    VStack {
                        Spacer()
                        Text((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)
                            .font(.footnote)
                            .rotationEffect(.degrees(-90))
                            .offset(y: 0)
                            .zIndex(1)
                        Rectangle()
                            .fill(Color.green)
                            .frame(width: 20, height:
                                Int((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)! < 10 ? 10 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)! < 20 ? 20 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)! < 30 ? 30 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)! < 50 ? 50 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "deathCnt") as! NSString as String)! < 100 ? 80 :
                                150))))
                        )
                        
                        Text((self.posts[index] as! NSDictionary).value(forKey: "gubun") as! NSString as String)
                            .font(.footnote)
                            .frame(height: 20)
                    }
                }
            }
        }
    }
}

struct CoronaChart_Preview: PreviewProvider {
    static var previews: some View {
        CoronaChart(posts: ViewController_CoronaGraph().posts)
    }
}


struct PharmacyInfo: View {
    var posts : NSMutableArray
    var station: WeatherStation
    
    var body: some View {
        TabView {
            CoronaChart(posts: self.posts)
                .tabItem({
                    Image(systemName: "snow")
                    Text("코로나 사망자 수")
                })
        }.frame(width: 400, height: 300)
    }
}

struct PharmacyInfo_Previews: PreviewProvider {
    static var previews: some View {
        PharmacyInfo(posts: ViewController_CoronaGraph().posts , station: WeatherInformation()!.stations[0])
    }
}


class ViewController_CoronaGraph: UIViewController, XMLParserDelegate {
    
    //xml파일을 다운로드 및 파싱하는 오브젝트
    var parser = XMLParser()
       
    // feed 데이터를 저장하는 mutable array
    var posts = NSMutableArray()

    // title과 data같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()

    // 저장 문자열 변수
    var gubun = NSMutableString()
    var deathCnt = NSMutableString()
 
    var ServiceKey = "OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D"
    
    // 코로나 발생현황
    var url : String =
    "http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19SidoInfStateJson?serviceKey=OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D&pageNo=1&numOfRows=1000"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
        updateSwiftUI()
    }
    
    func updateSwiftUI() {
        let stations = WeatherInformation()
        
        let swiftUIController = UIHostingController(rootView: PharmacyInfo(posts: posts, station: (stations?.stations[0])!))
        
        addChild(swiftUIController)
        swiftUIController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(swiftUIController.view)
        
        swiftUIController.view.centerXAnchor.constraint(
            equalTo: view.centerXAnchor).isActive = true
        swiftUIController.view.centerYAnchor.constraint(
            equalTo: view.centerYAnchor).isActive = true
        swiftUIController.didMove(toParent: self)

    }
    
    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(URL(string:url))!)!
        parser.delegate = self
        parser.parse()
    }

    // parser가 새로운 element를 발견하면 변수를 생성함
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            gubun = NSMutableString()
            gubun = ""
            deathCnt = NSMutableString()
            deathCnt = ""
        }
    }
                      
    // title과 pubDate를 발견하면 title1과 data에 완성한다.
       
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "gubun") {
            gubun.append(string)
        } else if element.isEqual(to: "deathCnt") {
            deathCnt.append(string)
        }
    }

    
    //element의 끝이나면, 하나의 카테고리 아이템이 끝난다면,
    //feed데이터를 dictionary에 저장
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            if !gubun.isEqual(nil) {
                elements.setObject(gubun, forKey: "gubun" as NSCopying)
            }
            if !deathCnt.isEqual(nil) {
                elements.setObject(deathCnt, forKey: "deathCnt" as NSCopying)
            }
            // post는 dictionary를 여러 개 가지는 그러한 array.
            posts.add(elements)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
