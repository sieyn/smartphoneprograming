//
//  DetailPharmacyViewController.swift
//  MyProject
//
//  Created by  kpugame on 2020/06/11.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit

class DetailPharmacyViewController: UITableViewController, XMLParserDelegate {
    @IBOutlet var detailTableView: UITableView!
    
    var url : String?
    
    var parser = XMLParser()
    
    let postsname : [String] = ["약국명", "주소", "전화번호", "개설일자", "우편번호"]
    var posts : [String] = ["","",""]
    
    var element = NSString()
    
    var yadmNm = NSMutableString() // 병원 이름
    var addr = NSMutableString()
    var telno = NSMutableString() // 번호
    var estbDd = NSMutableString()
    var postNo = NSMutableString()

    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }

    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(URL(string:url!))!)!
        parser.delegate = self
        parser.parse()
        detailTableView!.reloadData()
    }
    
    // parser가 새로운 element를 발견하면 변수를 생성함
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            posts = ["","","","",""]

            yadmNm = NSMutableString()
            yadmNm = ""
            addr = NSMutableString()
            addr = ""
            telno = NSMutableString()
            telno = ""
            estbDd = NSMutableString()
            estbDd = ""
            postNo = NSMutableString()
            postNo = ""

       }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "yadmNm") {
            yadmNm.append(string)
        } else if element.isEqual(to: "addr") {
            addr.append(string)
        } else if element.isEqual(to: "telno") {
            telno.append(string)
        } else if element.isEqual(to: "estbDd") {
            estbDd.append(string)
        } else if element.isEqual(to: "postNo") {
            postNo.append(string)
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if (elementName as NSString).isEqual(to: "item") {
            if !yadmNm.isEqual(nil) {
                posts[0] = yadmNm as String
            }
            if !addr.isEqual(nil) {
                posts[1] = addr as String
            }
            if !telno.isEqual(nil) {
                posts[2] = telno as String
            }
            if !estbDd.isEqual(nil) {
                posts[3] = estbDd as String
            }
            if !postNo.isEqual(nil) {
                posts[4] = postNo as String
            }
        }
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return posts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PharmacyCell", for: indexPath)
        cell.textLabel?.text = postsname[indexPath.row] // 11개 정보 타이틀
        cell.detailTextLabel?.text = posts[indexPath.row] // 11개 정보 값
        return cell
    }
    



}
