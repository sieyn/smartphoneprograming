//
//  ViewController.swift
//  MyProject
//
//  Created by  kpugame on 2020/05/28.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit
import Speech

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var transcribeButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var myTextView: UITextView!
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ko-KR"))!
    
    private var speechRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask: SFSpeechRecognitionTask?
    
    private let audioEngine = AVAudioEngine()

    @IBAction func startTranscribing(_ sender: Any) {
        transcribeButton.isEnabled = false
        stopButton.isEnabled = true
        try! startSession()
    }
    
    @IBAction func stopTranscribing(_ sender: Any) {
        // 오디오 엔진을 중지하고 다음 세션에 사용할 준비가 된 버튼의 상태를 구성.
        if audioEngine.isRunning {
            audioEngine.stop()
            speechRecognitionRequest?.endAudio()
            transcribeButton.isEnabled = true
            stopButton.isEnabled = false
        }
        
        // 음성인식한 내용을 pickerView에 반영
        switch (self.myTextView.text) {
        case "서울"  : self.pickerView.selectRow(0, inComponent: 0, animated: true)
            break
        case "경기" : self.pickerView.selectRow(1, inComponent: 0, animated: true)
            break
        case "대구" : self.pickerView.selectRow(2, inComponent: 0, animated: true)
            break
        case "부산" : self.pickerView.selectRow(3, inComponent: 0, animated: true)
            break
        case "세종시" : self.pickerView.selectRow(4, inComponent: 0, animated: true)
            break
        case "제주" : self.pickerView.selectRow(5, inComponent: 0, animated: true)
            break
        case "경남" : self.pickerView.selectRow(6, inComponent: 0, animated: true)
            break
        case "경북" : self.pickerView.selectRow(7, inComponent: 0, animated: true)
            break
        case "전남" : self.pickerView.selectRow(8, inComponent: 0, animated: true)
            break
        case "전북" : self.pickerView.selectRow(9, inComponent: 0, animated: true)
            break
        case "충남" : self.pickerView.selectRow(10, inComponent: 0, animated: true)
            break
        case "충북" : self.pickerView.selectRow(11, inComponent: 0, animated: true)
            break
        case "강원" : self.pickerView.selectRow(12, inComponent: 0, animated: true)
            break
        case "울산" : self.pickerView.selectRow(13, inComponent: 0, animated: true)
            break
        case "대전" : self.pickerView.selectRow(14, inComponent: 0, animated: true)
            break
        case "광주" : self.pickerView.selectRow(15, inComponent: 0, animated: true)
            break
        case "인천" : self.pickerView.selectRow(16, inComponent: 0, animated: true)
            break
        default: break
        }
    }
    
    
    // 아무 동작도 하지 않지만 이 메소드가 있어야 HospitalTableVIewController에서 unwind연결이 가능.
    @IBAction func doneToPickerViewController(segue:UIStoryboardSegue){
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToHospitalView" {
            if let navController = segue.destination as? UINavigationController {
                if let hospitalViewController = navController.topViewController as?
                    HospitalViewController {
                    hospitalViewController.url = url + sidoCd
                }
            }
        }
        
    }


    
    var pickerDataSource = ["서울", "경기", "대구", "부산", "세종시","제주","경남","경북","전남","전북","충남","충북","강원","울산","대전","광주", "인천"]

    var ServiceKey = "OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D"

    /*
    var url : String =
    "http://apis.data.go.kr/B551182/pubReliefHospService/getpubReliefHospList?serviceKey=OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D&pageNo=1&numOfRows=3000&spclAdmTyCd="
    */
    var url : String = "http://apis.data.go.kr/B551182/hospInfoService/getHospBasisList?serviceKey=OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D&pageNo=1&numOfRows=500&sidoCd="
    var sidoCd : String = "110000"

    
    
    
    // pickerView의 컴포넌트 개수 = 1
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // pickerView의 각 컴포넌트에 대한 row의 개수 = pickerDataSource 배열 원소 개수
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    // pickerView의 주어진 컴포넌트/row에 대한 데이터 = pickerDataSource 배열의 원소
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    // pickerView의 row 선택시 sgguCd를 변경
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            sidoCd = "110000"
        } else if row == 1 {
            sidoCd = "310000"
        } else if row == 2 {
            sidoCd = "230000"
        } else if row == 3 {
            sidoCd = "210000"
        } else if row == 4 {
            sidoCd = "410000"
        }else if row == 5 {
            sidoCd = "390000"
        }else if row == 6 {
            sidoCd = "380000"
        }else if row == 7 {
            sidoCd = "370000"
        }else if row == 8 {
            sidoCd = "360000"
        }else if row == 9 {
            sidoCd = "350000"
        }else if row == 10 {
            sidoCd = "340000"
        }else if row == 11 {
            sidoCd = "330000"
        }else if row == 12 {
            sidoCd = "320000"
        }else if row == 13 {
            sidoCd = "260000"
        }else if row == 14 {
            sidoCd = "250000"
        }else if row == 15 {
            sidoCd = "240000"
        }else if row == 16 {
            sidoCd = "220000"
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // pickerView의 데이터소스와 델리게이트를 ViewController로 설정
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
    }

    func authorizeSR() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.transcribeButton.isEnabled = true
                        
                case .denied:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition access denied by user", for: .disabled)
                        
                case .restricted:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition restricted on device", for: .disabled)
                    
                case .notDetermined:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition not authorized", for: .disabled)
                }
            }
        }
    }
        
    // 음성 인식을 수행하는 메소드 호출은 예외를 throw할 가능성이 있음
    func startSession() throws {
        // 이전 작업이 실행중인지 확인하고, 그렇지 않으면 작업을 취소.
        if let recognitionTask = speechRecognitionTask {
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
            
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSession.Category.record)
            
        // 이전에 선언된 변수에 개체를 할당
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
            
        // 성공적으로 만들어졌는지 확인하는 테스트. 실패한 경우 예외가 throw
        guard let recognitionRequest = speechRecognitionRequest else
        { fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed") }
            
        let inputNode = audioEngine.inputNode
            
        recognitionRequest.shouldReportPartialResults = true
            
        // 인식 작업이 초기화됨.
        // 텍스트뷰에 음성인식된 결과가 전달됨.
        speechRecognitionTask = speechRecognizer.recognitionTask(with:recognitionRequest){ result, error in
            var finished = false
                
            if let result = result {
                self.myTextView.text =
                    result.bestTranscription.formattedString
                finished = result.isFinal
            }
            // 그렇지 않으면 오디오 엔진이 중지되고, 오디오 노드에서 제거됨.
            if error != nil || finished {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                    
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                    
                self.transcribeButton.isEnabled = true
            }
        }
            
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format:recordingFormat) { (buffer: AVAudioPCMBuffer, when:AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
        }
            
        audioEngine.prepare()
        try audioEngine.start()
    }

    
    
}

