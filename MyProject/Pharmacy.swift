//
//  Hospital.swift
//  HospitalMap
//
//  Created by  kpugame on 2020/05/19.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import Foundation
import MapKit
// 위치와 같은 주소, 사전키 상수 포함
import Contacts

class Pharmacy: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    // helper method
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
    var markerTintColor: UIColor {
        return .purple
    }

}
