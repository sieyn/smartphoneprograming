//
//  ArtworkViews.swift
//  HonoluluArt
//
//  Created by  kpugame on 2020/05/19.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import MapKit

class PharmacyMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let pharmacy = newValue as? Pharmacy else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            markerTintColor = pharmacy.markerTintColor
            glyphText = String(pharmacy.title?.first! ?? "서")
        }
    }
}
