//
//  ViewController_SwiftUI1.swift
//  MyProject
//
//  Created by  kpugame on 2020/06/27.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit
import SwiftUI


struct HospitalChart: View {
    var posts : NSMutableArray
    
    var body: some View {
        VStack {
            Text("의사 총수")
            HStack {
                ForEach(0..<10) { index in
                    
                    VStack {
                        Spacer()
                        Text((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)
                            .font(.footnote)
                            .rotationEffect(.degrees(-90))
                            .offset(y: 0)
                            .zIndex(1)
                        Rectangle()
                            .fill(Color.green)
                            .frame(width: 20, height:
                                Int((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)! < 400 ? 20 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)! < 600 ? 50 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)! < 1000 ? 60 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)! < 1500 ? 80 :
                                (Int((self.posts[index] as! NSDictionary).value(forKey: "drTotCnt") as! NSString as String)! < 2000 ? 100 :
                                150))))
                        )
                        
                        Text((self.posts[index] as! NSDictionary).value(forKey: "yadmNm") as! NSString as String)
                            .font(.footnote)
                            .frame(height: 20)
                    }
                }
            }
        }
    }
}

struct HospitalChart_Preview: PreviewProvider {
    static var previews: some View {
        HospitalChart(posts: ViewController_HospitalGraph().posts)
    }
}

struct HospitalInfo: View {
    var posts : NSMutableArray
    var station: WeatherStation
    
    var body: some View {
        TabView {
            /*
            CoronaChart(posts: self.posts)
                .tabItem({
                    Image(systemName: "snow")
                    Text("코로나 사망자 수")
                })
             */
            HospitalChart(posts: self.posts)
                .tabItem({
                    Image(systemName: "snow")
                    Text("의사 총 수")
                })
 
        }.frame(width: 400, height: 300)
    }
}

struct HospitalInfo_Previews: PreviewProvider {
    static var previews: some View {
        HospitalInfo(posts: ViewController_HospitalGraph().posts , station: WeatherInformation()!.stations[0])
    }
}


class ViewController_HospitalGraph: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, XMLParserDelegate {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var tableView: UITableView!
    
    //xml파일을 다운로드 및 파싱하는 오브젝트
    var parser = XMLParser()
       
    // feed 데이터를 저장하는 mutable array
    var posts = NSMutableArray()

    // title과 data같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()

    // 저장 문자열 변수
    var yadmNm = NSMutableString() // 병원 이름
    var sidoCdNm = NSMutableString() // 경기
    var addr = NSMutableString()
    var drTotCnt = NSMutableString()
    
    var gubun = NSMutableString()
    var deathCnt = NSMutableString()
 
    // 병원 이름 변수와 utf8변수 추가
    var hospitalname = ""
    var hospitalname_utf8 = ""

    var pickerDataSource = ["서울", "경기", "대구", "부산", "세종시","제주","경남","경북","전남","전북","충남","충북","강원","울산","대전","광주", "인천"]

    var ServiceKey = "OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D"
    
    // 병원
    var url : String = "http://apis.data.go.kr/B551182/hospInfoService/getHospBasisList?serviceKey=OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D&pageNo=1&numOfRows=1000&sidoCd="
    
    // 코로나 발생현황
    var url2 : String =
    "http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19SidoInfStateJson?serviceKey=OaFVuHrkITE72siesjF2BP8PXFHN3BAxc4LepR9uCD%2FqpZOdfgAnf2zg%2FRgOnCLYxM%2BtaAoe%2F9G2daPj3HScCg%3D%3D&pageNo=1&numOfRows=1000"
    
    var sidoCd : String = "110000" {
        didSet {
            posts=[]
            beginParsing()
            updateSwiftUI()
        }
    }
    // pickerView의 컴포넌트 개수 = 1
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // pickerView의 각 컴포넌트에 대한 row의 개수 = pickerDataSource 배열 원소 개수
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    // pickerView의 주어진 컴포넌트/row에 대한 데이터 = pickerDataSource 배열의 원소
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    // pickerView의 row 선택시 sgguCd를 변경
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {
            sidoCd = "110000"
        } else if row == 1 {
            sidoCd = "310000"
        } else if row == 2 {
            sidoCd = "230000"
        } else if row == 3 {
            sidoCd = "210000"
        } else if row == 4 {
            sidoCd = "410000"
        }else if row == 5 {
            sidoCd = "390000"
        }else if row == 6 {
            sidoCd = "380000"
        }else if row == 7 {
            sidoCd = "370000"
        }else if row == 8 {
            sidoCd = "360000"
        }else if row == 9 {
            sidoCd = "350000"
        }else if row == 10 {
            sidoCd = "340000"
        }else if row == 11 {
            sidoCd = "330000"
        }else if row == 12 {
            sidoCd = "320000"
        }else if row == 13 {
            sidoCd = "260000"
        }else if row == 14 {
            sidoCd = "250000"
        }else if row == 15 {
            sidoCd = "240000"
        }else if row == 16 {
            sidoCd = "220000"
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
        beginParsing2()
        updateSwiftUI()
    }
    
    func updateSwiftUI() {
        let stations = WeatherInformation()
        
        let swiftUIController = UIHostingController(rootView: HospitalInfo(posts: posts, station: (stations?.stations[0])!))
        
        addChild(swiftUIController)
        swiftUIController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(swiftUIController.view)
        
        swiftUIController.view.centerXAnchor.constraint(
            equalTo: view.centerXAnchor).isActive = true
        swiftUIController.view.centerYAnchor.constraint(
            equalTo: view.centerYAnchor).isActive = true
        swiftUIController.didMove(toParent: self)

    }
    
    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(URL(string:url+sidoCd))!)!
        parser.delegate = self
        parser.parse()
        tableView!.reloadData()
    }
    func beginParsing2()
    {
        parser = XMLParser(contentsOf:(URL(string:url2))!)!
        parser.delegate = self
        parser.parse()
    }

    // parser가 새로운 element를 발견하면 변수를 생성함
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            yadmNm = NSMutableString()
            yadmNm = ""
            sidoCdNm = NSMutableString()
            sidoCdNm = ""
            drTotCnt = NSMutableString()
            drTotCnt = ""
            addr = NSMutableString()
            addr = ""
            gubun = NSMutableString()
            gubun = ""
            deathCnt = NSMutableString()
            deathCnt = ""
        }
    }
                      
    // title과 pubDate를 발견하면 title1과 data에 완성한다.
       
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "yadmNm") {
            yadmNm.append(string)
        } else if element.isEqual(to: "sidoCdNm") {
            sidoCdNm.append(string)
        } else if element.isEqual(to: "drTotCnt") {
            drTotCnt.append(string)
        } else if element.isEqual(to: "addr") {
            addr.append(string)
        } else if element.isEqual(to: "gubun") {
            gubun.append(string)
        } else if element.isEqual(to: "deathCnt") {
            deathCnt.append(string)
        }
    }

    
    //element의 끝이나면, 하나의 카테고리 아이템이 끝난다면,
    //feed데이터를 dictionary에 저장
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            if !yadmNm.isEqual(nil) {
                elements.setObject(yadmNm, forKey: "yadmNm" as NSCopying)
            }
            if !sidoCdNm.isEqual(nil) {
                elements.setObject(sidoCdNm, forKey: "sidoCdNm" as NSCopying)
            }
            if !drTotCnt.isEqual(nil) {
                elements.setObject(drTotCnt, forKey: "drTotCnt" as NSCopying)
            }
            if !addr.isEqual(nil) {
                elements.setObject(addr, forKey: "addr" as NSCopying)
            }
            if !gubun.isEqual(nil) {
                elements.setObject(gubun, forKey: "gubun" as NSCopying)
            }
            if !deathCnt.isEqual(nil) {
                elements.setObject(deathCnt, forKey: "deathCnt" as NSCopying)
            }
            // post는 dictionary를 여러 개 가지는 그러한 array.
            posts.add(elements)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value2, reuseIdentifier: nil)

        cell.textLabel?.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "yadmNm") as! NSString as String
        cell.detailTextLabel?.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "addr") as! NSString as String
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
