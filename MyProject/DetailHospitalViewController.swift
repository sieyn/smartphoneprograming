//
//  DetailHospitalTableViewController.swift
//  HospitalMap
//
//  Created by  kpugame on 2020/05/19.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit

class DetailHospitalViewController: UITableViewController, XMLParserDelegate {

    @IBOutlet var detailTableView: UITableView!
    
    var url : String?
    
    var parser = XMLParser()
    
    let postsname : [String] = ["병원명", "시도명", "시군구명", "홈페이지", "설립날짜", "분류", "우편번호"]
    var posts : [String] = ["","","","","","",""]
    
    var element = NSString()
    
    var yadmNm = NSMutableString() // 병원 이름
    var sidoCdNm = NSMutableString() // 경기
    var sgguCdNm = NSMutableString() // 화성시
    var hospUrl = NSMutableString() // 번호
    var estbDd = NSMutableString()
    var clCdNm = NSMutableString()
    var postNo = NSMutableString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }

    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(URL(string:url!))!)!
        parser.delegate = self
        parser.parse()
        detailTableView!.reloadData()
    }
    
    // parser가 새로운 element를 발견하면 변수를 생성함
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            posts = ["","","","","","",""]

            yadmNm = NSMutableString()
            yadmNm = ""
            sidoCdNm = NSMutableString()
            sidoCdNm = ""
            sgguCdNm = NSMutableString()
            sgguCdNm = ""
            hospUrl = NSMutableString()
            hospUrl = ""
            estbDd = NSMutableString()
            estbDd = ""
            clCdNm = NSMutableString()
            clCdNm = ""
            postNo = NSMutableString()
            postNo = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "yadmNm") {
            yadmNm.append(string)
        } else if element.isEqual(to: "sidoCdNm") {
            sidoCdNm.append(string)
        } else if element.isEqual(to: "sgguCdNm") {
            sgguCdNm.append(string)
        } else if element.isEqual(to: "hospUrl") {
            hospUrl.append(string)
        } else if element.isEqual(to: "estbDd") {
            estbDd.append(string)
        } else if element.isEqual(to: "clCdNm") {
            clCdNm.append(string)
        } else if element.isEqual(to: "postNo") {
            postNo.append(string)
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if (elementName as NSString).isEqual(to: "item") {
            if !yadmNm.isEqual(nil) {
                posts[0] = yadmNm as String
            }
            if !sidoCdNm.isEqual(nil) {
                posts[1] = sidoCdNm as String
            }
            if !sgguCdNm.isEqual(nil) {
                posts[2] = sgguCdNm as String
            }
            if !hospUrl.isEqual(nil) {
                posts[3] = hospUrl as String
            }
            if !estbDd.isEqual(nil) {
                posts[4] = estbDd as String
            }
            if !clCdNm.isEqual(nil) {
                posts[5] = clCdNm as String
            }
            if !postNo.isEqual(nil) {
                posts[6] = postNo as String
            }

        }
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return posts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalCell", for: indexPath)
        cell.textLabel?.text = postsname[indexPath.row] // 11개 정보 타이틀
        cell.detailTextLabel?.text = posts[indexPath.row] // 11개 정보 값
        return cell
    }
    



}
