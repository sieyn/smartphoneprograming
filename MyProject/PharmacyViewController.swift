//
//  PharmacyViewController.swift
//  MyProject
//
//  Created by  kpugame on 2020/06/09.
//  Copyright © 2020 OhSieun. All rights reserved.
//
//
//  HospitalViewController.swift
//  MyProject
//
//  Created by  kpugame on 2020/06/09.
//  Copyright © 2020 OhSieun. All rights reserved.
//

import UIKit

class PharmacyViewController: UITableViewController, XMLParserDelegate {
    @IBOutlet var tbData: UITableView!
    
    var url : String?
    
    //xml파일을 다운로드 및 파싱하는 오브젝트
    var parser = XMLParser()
       
    // feed 데이터를 저장하는 mutable array
    var posts = NSMutableArray()
       
    // title과 data같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
       
    // 저장 문자열 변수

    var yadmNm = NSMutableString() // 병원 이름
    var sidoCdNm = NSMutableString() // 경기
    var sgguCdNm = NSMutableString()
    var emdongNm = NSMutableString()
    
    var addr = NSMutableString()
    var XPos = NSMutableString()
    var YPos = NSMutableString()
    
    // 이름 변수와 utf8변수 추가
    var pharmacyname = ""
    var pharmacyname_utf8 = ""
    
    struct Pharmacy {
        let location : String
        let name : String
    }
    var detailViewController: DetailHospitalViewController? = nil
    
    var filteredPharmacys = [Pharmacy]()
    var pharmacys = [Pharmacy]()
    let searchController = UISearchController(searchResultsController: nil)

    
    //prepare 메소드는 segue 실행될 때 호출되는 메소드
    // 시작화면에서 만들어줘야 하는 것.
    // MapViewController에 posts정보 정달.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToMapView" {
            if let mapViewController = segue.destination as? MapViewController {
                mapViewController.posts = posts
            }
        }
        
        // 선택한 table view row의 병원명을 utf8로 코딩
        if segue.identifier == "segueToPharmacyDetail" {
            if let cell = sender as? UITableViewCell {
                let indexPath = tableView.indexPath(for: cell)
                pharmacyname = (posts.object(at: (indexPath?.row)!) as AnyObject).value(forKey: "yadmNm") as! NSString as String
                pharmacyname_utf8 = pharmacyname.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                
                if let detailPharmacyViewController = segue.destination as? DetailPharmacyViewController {
                    detailPharmacyViewController.url = url! + "&yadmNm=" + pharmacyname_utf8
                }
            }
        }
        
        if segue.identifier == "segueToPharmacyGraph" {
            if let navController = segue.destination as? UINavigationController {
                if let ViewController = navController.topViewController as?
                        ViewController_CoronaGraph {
                        ViewController.url = url!
                    }
                }

        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredPharmacys = pharmacys.filter({( pharmacy : Pharmacy) -> Bool in
            return pharmacy.name.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Location"
        navigationItem.searchController = searchController
        definesPresentationContext = true

        beginParsing()
    }
     
    func beginParsing()
    {
        posts = []
        parser = XMLParser(contentsOf:(URL(string:url!))!)!
        parser.delegate = self
        parser.parse()
        tbData!.reloadData()
       }
           
    // parser가 새로운 element를 발견하면 변수를 생성함
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            yadmNm = NSMutableString()
            yadmNm = ""
            sidoCdNm = NSMutableString()
            sidoCdNm = ""
            sgguCdNm = NSMutableString()
            sgguCdNm = ""
            emdongNm = NSMutableString()
            emdongNm = ""
            addr = NSMutableString()
            addr = ""
            XPos = NSMutableString()
            XPos = ""
            YPos = NSMutableString()
            YPos = ""
        }
    }
                      
    // title과 pubDate를 발견하면 title1과 data에 완성한다.
       
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if element.isEqual(to: "yadmNm") {
            yadmNm.append(string)
        } else if element.isEqual(to: "sidoCdNm") {
            sidoCdNm.append(string)
        } else if element.isEqual(to: "sgguCdNm") {
            sgguCdNm.append(string)
        } else if element.isEqual(to: "emdongNm") {
            emdongNm.append(string)
        } else if element.isEqual(to: "addr") {
            addr.append(string)
        } else if element.isEqual(to: "XPos") {
            XPos.append(string)
        } else if element.isEqual(to: "YPos") {
            YPos.append(string)
        }
    }
    
    //element의 끝이나면, 하나의 카테고리 아이템이 끝난다면,
    //feed데이터를 dictionary에 저장
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "item") {
            if !yadmNm.isEqual(nil) {
                elements.setObject(yadmNm, forKey: "yadmNm" as NSCopying)
            }
            if !sidoCdNm.isEqual(nil) {
                elements.setObject(sidoCdNm, forKey: "sidoCdNm" as NSCopying)
            }
            if !sgguCdNm.isEqual(nil) {
                elements.setObject(sgguCdNm, forKey: "sgguCdNm" as NSCopying)
            }
            if !emdongNm.isEqual(nil) {
                elements.setObject(emdongNm, forKey: "emdongNm" as NSCopying)
            }
            if !addr.isEqual(nil) {
                elements.setObject(addr, forKey: "addr" as NSCopying)
            }
            if !XPos.isEqual(nil) {
                elements.setObject(XPos, forKey: "XPos" as NSCopying)
            }
            if !YPos.isEqual(nil) {
                elements.setObject(YPos, forKey: "YPos" as NSCopying)
            }

            // post는 dictionary를 여러 개 가지는 그러한 array.
            posts.add(elements)
        }
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // post라는 array를 만든 뒤, 테이블뷰로 보여줄 것.
    // tableview가 필요한 메소드.
    //row의 개수는 posts 배열 원소의 개수
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return posts.count
    }
        
    // 테이블뷰 셀의 내용은 title과 subtitle을 posts 배열의 원소(dictionary)에서 title과 date에 해당하는
    // value로 설정.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
        cell.textLabel?.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "yadmNm") as! NSString as String
//        cell.detailTextLabel?.text = ((posts.object(at: indexPath.row) as AnyObject).value(forKey: "sidoCdNm") as! NSString as String) + " " + ((posts.object(at: indexPath.row) as AnyObject).value(forKey: "sgguCdNm") as! NSString as String) + " " + ((posts.object(at: indexPath.row) as AnyObject).value(forKey: "emdongNm") as! NSString as String)
        cell.detailTextLabel?.text = (posts.object(at: indexPath.row) as AnyObject).value(forKey: "addr") as! NSString as String
        return cell // as UITableViewCell
    }


}

extension PharmacyViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
